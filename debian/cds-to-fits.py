#!/usr/bin/env python

from glob import glob
from astropy.io import ascii, fits

def file_chain(pattern, cols):
    fnames = glob(pattern)
    fnames.sort()
    for fn in fnames:
        print(fn)
        with open(fn) as f:
            for line in f:
                yield ','.join(line[slice(*c)] for c in cols)

cat = ascii.read(list(file_chain('tyc2.dat.*',
                                 [(15,27), (28,40), (123,129)]))
                 + list(file_chain('suppl_1.dat',
                                   [(15,27), (28,40), (96,102)])))
print(cat[:3])
print("Total entries: {}".format(len(cat)))

tbhdu = fits.BinTableHDU.from_columns([
    fits.Column(name='RA', format='E', array=cat['col1']),
    fits.Column(name='DEC', format='E', array=cat['col2']),
    fits.Column(name='MAG_VT', format='E', array=cat['col3']),
])
tbhdu.writeto('tyc2.fits')

