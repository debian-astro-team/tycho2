#!/usr/bin/env python3

import sys
from astropy.io import fits

fname = sys.argv[1]
ext = [int(i) for i in sys.argv[2].split(',')]
key = sys.argv[3]
value = sys.argv[4]

with fits.open(fname, memmap=True, mode='update') as f:
    for e in ext:
        f[e].header[key] = value
